﻿
namespace DEP_OPT
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Secuencias = new System.Windows.Forms.DataGridView();
            this.Ejecutar = new System.Windows.Forms.Button();
            this.secuenciasRecursos = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.Secuencias)).BeginInit();
            this.SuspendLayout();
            // 
            // Secuencias
            // 
            this.Secuencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Secuencias.Location = new System.Drawing.Point(12, 180);
            this.Secuencias.Name = "Secuencias";
            this.Secuencias.RowTemplate.Height = 25;
            this.Secuencias.Size = new System.Drawing.Size(776, 258);
            this.Secuencias.TabIndex = 0;
            this.Secuencias.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Ejecutar
            // 
            this.Ejecutar.Location = new System.Drawing.Point(21, 120);
            this.Ejecutar.Name = "Ejecutar";
            this.Ejecutar.Size = new System.Drawing.Size(75, 23);
            this.Ejecutar.TabIndex = 1;
            this.Ejecutar.Text = "Ejecutar";
            this.Ejecutar.UseVisualStyleBackColor = true;
            this.Ejecutar.Click += new System.EventHandler(this.Ejecutar_click);
            // 
            // secuenciasRecursos
            // 
            this.secuenciasRecursos.FormattingEnabled = true;
            this.secuenciasRecursos.Location = new System.Drawing.Point(522, 62);
            this.secuenciasRecursos.Name = "secuenciasRecursos";
            this.secuenciasRecursos.Size = new System.Drawing.Size(121, 23);
            this.secuenciasRecursos.TabIndex = 2;
            this.secuenciasRecursos.SelectedIndexChanged += new System.EventHandler(this.secuenciasRecursos_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.secuenciasRecursos);
            this.Controls.Add(this.Ejecutar);
            this.Controls.Add(this.Secuencias);
            this.Name = "Form1";
            this.Text = "DEP_OPT";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Secuencias)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Secuencias;
        private System.Windows.Forms.Button Ejecutar;
        private System.Windows.Forms.ComboBox secuenciasRecursos;
    }
}

